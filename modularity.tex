\section{Taylor's conjecture}

In this section, we prove Theorem~\ref{thm:taylors-conjecture-introduction}
about Taylor's conjecture on congruence modular varieties. 
Congruence modular varieties have been thoroughly investigated, and they have
many nice properties, nevertheless we need only the definition and the following
Mal'cev characterization of these varieties by A.~Day \cite{day69}:

\begin{theorem} \label{thm:day}
The following are equivalent for any variety $\var V$.
\begin{enumerate}
  \item Every algebra in $\var V$ has modular congruence lattice;
  \item there exists $n$, and quaternary terms $d_0,\dots,d_n$ 
    such that the following identities are satisfied in $\var V$:
    \begin{align*}
      d_0(x,y,z,w) &\equals x \text{ and } d_n(x,y,z,w) \equals w, \\
      d_i(x,y,y,z) &\equals d_{i+1}(x,y,y,z) \text{ for even $i$,} \\
      d_i(x,x,y,y) &\equals d_{i+1}(x,x,y,y) \text{ and }
      d_i(x,y,y,x) \equals d_{i+1}(x,y,y,x) \text{ for odd $i$}.
    \end{align*}
\end{enumerate}
\end{theorem}

A~sequence of terms satisfying the identities in (2) is referred to as
\emph{Day terms}; we will also say that some functions (or polymorphisms) are
\emph{Day functions} if they satisfy these identities.

%\begin{figure}[ht]
%  \begin{tikzpicture}
%    \node (0) at (0,0) {$x$};
%    \node (1) at (5,0) {$y$};
%    \node (2) at (5,5) {$z$};
%    \node (3) at (0,5) {$w$};
%
%    %\draw (0) edge [bend right, color=red] (3);
%    \node (d1) at (.6,1) {$d_1$};
%    \node (d2) at (.8,2) {$d_2$};
%    \node (d3) at (.8,3) {$d_3$};
%    \node (d4) at (.6,4) {$d_4$};
%
%    \draw (0) edge node[midway,right] {$\gamma$} (d1);
%    \draw (d1) edge [bend left] node[midway,left] {$\alpha$} (d2);
%    \draw (d1) edge [bend right] node[midway,right] {$\beta$} (d2);
%    \draw (d2) edge node[midway,right] {$\gamma$} (d3);
%    \draw (d3) edge [bend left] node[midway,left] {$\alpha$} (d4);
%    \draw (d3) edge [bend right] node[midway,right] {$\beta$} (d4);
%    \draw [line cap=round, line width=1pt,
%      dash pattern = on .01pt off 3pt on .01pt off 3pt on .1pt off 20pt,
%      dash phase = 23pt] (d4) edge  (3);
%
%    \draw (0) edge [bend left] node[midway,left] {$\alpha$} (3);
%    \draw (1) edge [bend left] node[midway,left] {$\alpha$} (2);
%
%    \draw (1) edge [bend right] node[midway,right] {$\gamma$} (2);
%
%    \draw (0) edge node[midway,above] {$\beta$} (1);
%    \draw (2) edge node[midway,above] {$\beta$} (3);
%  \end{tikzpicture}
%  \caption{Day terms}
%  %{$\alpha = \Cg\{(x,w)(z,y)\}$, $\beta = \Cg\{(x,y),(z,w)\}$, and $\gamma =
%  %\Cg\{z,w\}$}
%\end{figure}

\subsection{Pentagons}

For the description of a~cofinal chain of clones in the complement of the
filter of clones containing Day terms, we will use relational structures
of the form $\rel P = (P; \alpha, \beta, \gamma)$ where $\alpha$, $\beta$, and
$\gamma$ are equivalence relations on $P$ that do not satisfy the modularity
law.
A~very similar structures have been used in \cite{bova.chen.valeriote13} to
prove that the problem of comparison of pp-formulae is coNP-hard for algebras
that do not generate congruence modular varieties; it was also used in
\cite{mcgarry09}. The following definition of a~pentagon is almost identical to
the one in \cite{bova.chen.valeriote13} (we focus on those pentagon which are by
themselves `interesting'). For our purpose we need pentagons of even more
special shape; we call them special and very special pentagons.

\begin{definition} A~\emph{pentagon} is a~relational structure
$\rel P$ in the signature $\{\alpha,\beta,\gamma\}$ with three binary relations
that are all equivalence relations on $P$ satisfying:
\begin{itemize}
  \item $\alpha^{\rel P} \meet \beta^{\rel P} = 0_P$,
  \item $\alpha^{\rel P} \circ \beta^{\rel P} = 1_P$,
  \item $\gamma^{\rel P} \join \beta^{\rel P} = 1_P$, and
  \item $\gamma^{\rel P} < \alpha^{\rel P}$.
\end{itemize}
\end{definition}

The first two items in this definition ensure that every pentagon naturally
factors as a~direct product $P = A \times B$ in such a~way that $\alpha$ and
$\beta$ are kernels of projections on the first and the second coordinate,
respectively. In this setting, $\gamma$ is an equivalence relation that relates
some pairs of the form $((a,b),(a,c))$. For an~equivalence $\gamma$ on a~product
$A\times B$, and $a \in A$, we define $\gamma^a$ to be the following equivalence
on $B$:
\[
  \gamma^a := \{ (b,c) : ((a,b),(a,c)) \in \gamma \}.
\]
A~pentagon on the set $A \times B$ (with $\alpha$ and $\beta$ being the two
kernels of projections) is said to be \emph{special} if $\gamma^a$ for $a\in A$
gives exactly two distinct congruences with one of them being the full
congruence on $B$, i.e., there exists $\eta < 1_B$ such that
\[
  \{ \gamma^a : a\in A \} = \{ 1_B, \eta \}.
\]
Such~pentagon is \emph{very special} if the above is true for $\eta = 0_B$. When
defining a~special, or a~very special pentagon $\rel P$ on a~product $A\times
B$, we will usually speak only about the relation $\gamma^{\rel P}$ since the
other two relations are implicitly given as the kernels of projections.

In this section we will use pentagons $\rel P_0$ and $\rel P_\kappa$ where
$\kappa$ is an infinite cardinal. All of these pentagons are either themselves
very special, or isomorphic to a~very special pentagon.

\begin{definition} \label{def:pentagon_0}
We define $\rel P_0$ as the~smallest possible pentagon. In detail: $P_0 =
\{0,1,2,3\}$ and $\alpha^{\rel P_0}$, $\beta^{\rel P_0}$, and $\gamma^{\rel
P_0}$ are equivalences defined by partitions $12|03$, $01|23$, and $12|0|3$,
respectively.

\begin{figure}[ht]
  \begin{tikzpicture}
    \node (0) at (0,0) {0};
    \node (1) at (2.5,0) {1};
    \node (2) at (2.5,2.5) {2};
    \node (3) at (0,2.5) {3};

    \draw (0) edge [bend left] node[midway,left] {$\alpha$} (3);
    \draw (1) edge [bend left] node[midway,left] {$\alpha$} (2);

    \draw (1) edge [bend right] node[midway,right] {$\gamma$} (2);

    \draw (0) edge node[midway,above] {$\beta$} (1);
    \draw (2) edge node[midway,above] {$\beta$} (3);
  \end{tikzpicture}
  \caption{The pentagon $\rel P_0$}
\end{figure}
\end{definition}

The pentagon $\rel P_0$ is isomorphic to a~very special pentagon on the set $P =
\{0,1\} \times \{0,1\}$ with $\gamma^0 = 0_{\{0,1\}}$ and $\gamma^1 =
1_{\{0,1\}}$; an isomorphism is given by the map: $0 \mapsto (0,0)$, $1 \mapsto
(1,0)$, $2 \mapsto (1,1)$, and $3 \mapsto (0,1)$.


\begin{definition} \label{def:pentagon_kappa}
For an~infinite cardinal $\kappa$, we define a~pentagon $\rel P_\kappa$: Fix
$U_\kappa \subseteq \kappa$ with $|U_\kappa| = |\kappa \setminus U_\kappa| =
\kappa$, and define $P_\kappa = \kappa \times \kappa$, $\alpha^{\rel P_\kappa}$
and $\beta^{\rel P_\kappa}$ to be the kernels of the first and the second
projection, respectively, and
\[
  \gamma^{\rel P_\kappa} = \{ ((a,b),(a,c)) : a,b,c\in\kappa
    \text{ such that } a \in U_\kappa \text{ or } b = c \}
.\]
\end{definition}

\subsection{Coloring and linear varieties}

Using Day terms, we obtain a~characterization of congruence non-modular
varieties by the means of coloring by the pentagon $\rel P_0$ defined above.

\begin{proposition} \label{prop:coloring-for-modularity}
A~variety $\var V$ does not have Day terms if and only if it has strongly $\rel
P_0$-colorable terms.
\end{proposition}

\begin{proof}
  Let $\rel F$ denote the structure obtained from the free structure in $\var
  V$ generated by $\rel P_0$ by replacing every its relation by its transitive
  closure, and note that all three relations of $\rel F$ are equivalences (see
  Lemma~\ref{lem:coloring-by-equivalences}), and also that $c\colon F \to P_0$ is
  a~coloring if and only if it is a~homomorphism from $\rel F$ to $\rel P_0$
  (see Lemma~\ref{lem:coloring-by-quosets}). To simplify the notation we
  identify a~4-ary term $d$ with the element \hbox{$d(x_0,x_1,x_2,x_3) \in F$}.

  First, we prove the implication from left to right. For a~contradiction,
  suppose that $\var V$ has Day terms $d_0,\dots,d_n$, and that there is
  a~strong coloring $c\colon \rel F \to \rel P_0$.  Any strong coloring $c$ maps
  $x_0$, and therefore also $d_0$, to $0$, and it satisfies \( (
  c(d_i),c(d_{i+1}) ) \in \gamma \) for even $i$, and \( ( c(d_i),c(d_{i+1}) )
  \in \alpha \meet \beta \) for odd $i$; this follows from Day identities that
  can be reformulated as: $(d_i,d_{i+1}) \in \gamma^{\rel F}$ for even $i$ and
  $(d_i,d_{i+1}) \in \alpha^{\rel F} \meet \beta^{\rel F}$ for odd $i$.  Using
  these observations, we obtain by induction on $i$ that $c$ maps $d_i$ to $0$
  for all $i$, in particular $c(x_3) = c(d_n) = 0$. This gives us
  a~contradiction with $c(x_3) = 3$.

  For the converse, we want to prove that Day terms are the only obstruction for
  having strongly $\rel P_0$-colorable terms. We will do that by defining
  a~valid strong coloring of terms of any $\var V$ that does not have Day terms.
  Observe that even in this case we can repeat the argumentation from the above
  paragraph to get that $c(f) = 0$ for any $f$ that is connected to $x_0$ by
  a~Day-like chain, i.e., terms $d_0$, \dots, $d_n$ satisfying Day identities
  where $d_n \equals x_3$ is replaced by $d_n \equals f$. Therefore, we put
  $c(f) = 0$ if this is the case.
  Furthermore, for any $f$ with $(f,g) \in \beta^{\rel F}$ for some $g$ having
  the property above, we have $c(f) \in \{0,1\}$. We put $c(f) = 1$ if this is
  the case and we have not defined the value $c(f)$ yet. Similarly, if $(f,g)
  \in \alpha^{\rel F}$ for some $g$ with $c(g) = 0$ and $c(g)$ undefined, we put
  $c(f) = 3$ following the rule that $c(f) \in \{0,3\}$. Note that in this step
  we have defined $c(x_3) = 3$ since $(x_0,x_3) \in \alpha^{\rel F}$. Finally,
  for the remaining $f \in F$ we put $c(f) = 2$. This definition is summarized
  as follows:
  $c(f) = 0$ if $(f,x_0) \in (\freeclo\alpha \meet \freeclo\beta) \join
    \freeclo \gamma $, otherwise:
  \[
    c(f) = \begin{cases}
      1 & \text{if $(f,g) \in \freeclo\beta$ for some $g$ with $c(g) = 0$,}\\
      3 & \text{if $(f,g) \in \freeclo\alpha$ for some $g$ with $c(g) = 0$, 
          and} \\
      2 & \text{in all remaining cases.}
    \end{cases}
  \]
  Note that if $f$ satisfies both the first and the second row of the
  above, say there are $g_1$, $g_2$ with $c(g_i) = 0$, $(f,g_1) \in
  \beta^{\rel F}$, and $(f,g_2) \in \alpha^{\rel F}$, then also $(f,g_1) \in
  \alpha^{\rel F}$ since
  \(
    (\freeclo\alpha \meet \freeclo\beta) \join \freeclo \gamma
    \leq \freeclo \alpha
  \), and $c(f)$ is assigned value $0$ in the first step. Therefore, $c$ is
  well-defined.

  We claim that $c$ defined this way is a~homomorphism from $\rel F$ to
  $\rel P_0$, and therefore a~coloring. To prove that we need to show that $c$
  preserves all $\alpha$, $\beta$, and $\gamma$. First, let $(f,g) \in
  \alpha^{\rel F}$.
  Then either there is $h\in F$ with $c(h) = 0$ in the
  $\alpha^{\rel F}$-class of $f$ and $g$, and both $c(f)$ and $c(g)$ are
  assigned values in $\{0,3\}$, or there is no such $h$ and both $c(f)$ and
  $c(g)$ are assigned values in $\{1,2\}$. Either way $(c(f),c(g)) \in
  \alpha^{\rel P_0}$. The same argument can be used for showing $(c(f),c(g)) \in
  \beta^{\rel P_0}$ given that $(f,g) \in \beta^{\rel F}$. Finally, suppose that
  $(f,g) \in \gamma^{\rel F}$. If $c(f) \in \{1,2\}$, we immediately get that
  $c(g) \in \{1,2\}$ since $c$ preserves $\alpha$. If this is not the case, and
  $f$ and $g$ are assigned values that are not in the same class of $\gamma^{\rel
  P_0}$, then $c(f) = 0$ and $c(g) = 3$, or vice-versa. But this implies that
  $(f,g) \notin \gamma^{\rel F}$ as otherwise both would be assigned the value
  $0$. This concludes that $c$ is a~coloring.

  We are left to prove that $c$ is strong, i.e., $c(x_i) = i$ for $i=0,1,2,3$. 
  We know (from the defintion of $c$) that $c(x_0) = 0$ and $c(x_3) = 3$.
  Now, $x_1$ and $x_2$ are assigned values that are in the
  same class of $\gamma^{\rel P_0}$, and moreover $(c(x_1),c(x_0))\in
  \beta^{\rel P_0}$ and $(c(x_3),c(x_2))\in \beta^{\rel P_0}$. This leaves us
  with the only option: $c(x_1) = 1$ and $c(x_2) = 2$.
\end{proof}

Note that in the previous proof we use the same three congruences ($\alpha^{\rel
F}$, $\beta^{\rel F}$, and $\gamma^{\rel F}$) of the free algebra generated by
a~four element set that also appeared in the original proof of Day's results.
This is not a pure coincidence, we will use a~very similar method of deriving
a~suitable structure for coloring in Section~\ref{sec:n-permutability}.

\subsection{Idempotent varieties}
  \label{sec:modularity.idempotent}

In this subsection, we prove a~generalization of the following theorem of McGarry
\cite{mcgarry09} for idempotent varieties that do not need to be locally finite. 

\begin{theorem}[\cite{mcgarry09}]
A~locally finite idempotent variety is not congruence modular if and
only if it contains and algebra compatible with a~special pentagon.
\qed
\end{theorem}

Throughout this proof we will work with several variants of special and
very special pentagons $\rel P$ defined on a~product $A \times B$ for two algebras $\al
A$, $\al B$ in our idempotent variety, and in particular with the corresponding
relations $\gamma^{\rel P}$.
We say that $\gamma$ is a~\emph{modularity blocker} if $\rel P =
(A\times B; \alpha, \beta, \gamma)$ is a~special pentagon given that $\alpha$
and $\beta$ are kernels of the two projections, and $\gamma$ is a~\emph{special
modularity blocker} if $\rel P$ is a~very special pentagon.

The first step of the proof coincides with McGarry's proof. Nevertheless, we
present an alternative proof for completeness.

\begin{lemma}
Let $\mathcal V$ be an~idempotent variety, and $\al F$ the free algebra in $\var
V$ generated by $\{x,y\}$. Then $\mathcal V$ is congruence modular if and only if
\[
  \big((y,x),(y,y)\big) \in \Cg_{\al F\times \al F} \{ \big((x,x),(x,y)\big) \}.
    \eqno{(*)}
\]
\end{lemma}

\begin{proof}
Given that $\var V$ is congruence modular, one obtains $(*)$ from the modularity
law applied on the two kernels of projections and the congruence on the right
hand side of $(*)$.

To show that $(*)$ also implies congruence modularity, we will prove that $(*)$
is not true in any $\mathcal V$ that is idempotent and not congruence modular.
First, we consider the free algebra $\al F_4$ in $\var V$ generated by the
four-element set $\{x_0,x_1,x_2,x_3\}$ and its congruences $\alpha = \Cg
\{(x_0,x_3),(x_1,x_2)\}$, $\beta = \Cg\{(x_0,x_1),(x_2,x_3)\}$, and $\gamma = \Cg\{ (x_1,x_2) \}$.
Note that this is the structure $\rel F$ that has been used in the proof of
Proposition~\ref{prop:coloring-for-modularity}. Either from this proposition, or
by a~standard argument from the proof of Day's result, we obtain that $\alpha$,
$\beta$, and $\gamma$ don't satisfy the modularity law, and in particular,
$(x_0,x_3) \notin \gamma \join (\alpha \meet \beta)$.

Next, we shift this property to the second power of the two-generated free
algebra; consider the homomorphism $h\colon \al F_4 \to \al F \times \al F$
defined on the generators by
$x_0 \mapsto (y,x)$, $x_1 \mapsto (x,x)$, $x_2 \mapsto (x,y)$, and $x_3 \mapsto (y,y)$.
The homomorphism $h$ is surjective, since for every two binary idempotent terms
$t,s$ we have
\(
  h\big( t(s(x_1,x_2),s(x_0,x_3)) \big) = \big( t(x,y), s(x,y) \big)
\).
Finally, since the kernel of $h$ is $\alpha \meet \beta$, we get that 
\[
  h^{-1}\big(\Cg_{\al F\times \al F} \{((x,x),(x,y))\}\big)
    = \gamma \join (\alpha \meet \beta) \notni (x_0,x_3),
\]
and consequently $((y,x),(y,y)) = (h(x_0),h(x_3)) \notin \Cg_{\al F\times \al F}
\{((x,x),(x,y))\}$.
\end{proof}

\begin{lemma} \label{lem:idempotent-modularity-blocker}
Let $\mathcal V$ be an~idempotent variety which is not congruence modular, and
$\al F = \al F_{\mathcal V} (x,y)$. Then there is a~modularity blocker $\gamma$
in $\al F\times \al F$ such that $((x,x),(x,y)) \in \gamma$ and $((y,x),(y,y))
\notin \gamma$,
\end{lemma}

\begin{proof}
Let $\gamma_0 = \Cg_{\al F\times \al F} \{ \big((x,x),(x,y)\big) \}$. From the previous
lemma, we know that $((y,x),(y,y)) \notin \gamma_0$. Let $\eta$ be a~maximal
equivalence relation on $F$ such that
\[
  \big((y,x),(y,y)\big) \notin
    \gamma_0 \join \Cg \{ \big((y,a),(y,b)\big) : (a,b) \in \eta \}
\]
(such equivalence exists from Zorn's lemma). We claim that the equivalence on
the right-hand side, let us call it $\gamma$, is a~modularity blocker. This is
proven in two steps:

{\def\qedsymbol{$\triangle$}
\begin{claim} $\gamma^p \geq \eta$ for all $p\in F$.
\end{claim}

Let $f$ be binary term such that $f(x,y) = p$.
Observe that $\gamma^x = 1_F \geq \eta$ and $\gamma^y = \eta$, therefore we
obtain that
\(
  \big((p,a),(p,b)\big) =
    \big(f\big((x,a),(y,a)\big),f\big((x,b),(y,b)\big)\big) \in \eta
\)
for all
$(a,b) \in \eta$. Which shows that $\gamma^p \geq \eta$. \qed

\begin{claim} If $\gamma^p > \eta$ for some $p\in F$ then $\gamma^p = 1_F$.
\end{claim}

Let $e = e' \times 1_{\al F}$, where $e'\colon \al F \to \al F$ is the
homomorphism defined by $x\mapsto x$ and $y\mapsto p$, and consider the
congruence $\gamma_1 = e^{-1}(\gamma)$. From this definition, we obtain that
$\gamma_1^y \geq \gamma^p > \eta$ and moreover $\gamma_1 \geq \gamma_0$ since
$(e(x,x),e(x,y)) = ((x,x),(x,y)) \in \gamma$.  Therefore, from the maximality
of $\eta$, we obtain that $((y,x),(y,y)) \in \gamma_1$, and as a~consequence
thereof, $((p,x),(p,y)) = (e(y,x),e(y,y)) \in \gamma$. This shows that
$\gamma^p = 1_\al F$ and completes the proof of the second claim. \qed
}

By combining both claims, we get that $\gamma^p \in \{ \eta, 1_F \}$, therefore
$\gamma$ is a~modularity blocker.
\end{proof}
The above proof was inspired by the proof of Lemma 2.8
from~\cite{kearnes.tschantz07}.

\begin{corollary} \label{cor:idempotent-special-pentagon}
  Every idempotent variety that is not congruence modular is
  compatible with a~very special pentagon.
\end{corollary}

\begin{proof}
Following the notation of the previous lemma, we know that $(F\times F,\alpha,
\beta,\gamma)$ is a~special pentagon for kernels of projections $\alpha$,
$\beta$.
To obtain a~very special pentagon $\rel P$, first observe that $\gamma^y$ is
a~congruence on $\al F$: by idempotence we get that $\{y\} \times \al F$ is
a~subalgebra of $\al F^2$ isomorphic to $\al F$, and $\gamma^y$ is the image of
the restriction of $\gamma$ to $\{y\} \times \al F$ under this isomorphism.
We set $\al A = \al F$, $\al B = \al F / \gamma^y$, and $P = A \times B$.
Finally, the relation $\gamma^{\rel P}$ is defined as the image of $\gamma$
under the natural epimorphism from $\al F^2$ to $\al A\times \al B$.
\end{proof}

The next step is to show that we can increase the size of compatible very
special pentagons. In other words, that every variety compatible with a~very
special pentagon is compatible with $\rel P_\kappa$'s for all big enough
cardinals $\kappa$.

\begin{proposition} \label{prop:huge-pentagons}
If a~variety $\var V$ contains an algebra compatible with a~very special
pentagon $\rel P$, then it contains an algebra compatible with $\rel P_\kappa$
for all $\kappa \geq \aleph_0 + |P|$.
\end{proposition}

\begin{proof}
  Let $\al A$ and $\al B$ be two algebras in $\var V$ such that $\rel P$ is
  compatible with $\al A \times \al B$, let $U\subseteq A$ denote the set
  $\{ a\in A : \gamma^a = 1_B \}$, and let $\kappa \geq \aleph_0 + |P|$ be
  a~cardinal.

  We will construct a~pentagon which is isomorphic to $\rel P_\kappa$ by
  a~variation on Tarski's construction described in
  Section~\ref{sec:tarski-construction}.  Let $\al A_\lambda$ and $U_\lambda$ be
  the algebra and its subset obtained by the construction from $\al A$ and $U$
  in $\lambda$ steps, and let $\al B_\lambda$ be the algebra obtained by the
  construction from $\al B$ in $\lambda$ steps. We define an~equivalence
  relation $\gamma_\lambda$ on $A_\lambda \times B_\lambda$ by
  \[
    \gamma_\lambda = \{ ((a,b), (a',b')) :
      \text{$a = a'$ and if $a\not\in U_\lambda$ then $b = b'$} \}.
  \]
  We will prove that $\gamma_\lambda$ is a~congruence of $\al A_\lambda\times
  \al B_\lambda$ by a~transfinite induction: $\gamma_0 = \gamma$ is
  a~congruence. For the induction step, suppose that $\gamma_\lambda$ is
  a~congruence. The algebras $\al A_{\lambda + 1}$ and $\al B_{\lambda +1}$ are
  defined as being isomorphic to the second powers of $\al A_\lambda$ and $\al
  B_\lambda$ respectively. Let us suppose that they are in fact the second
  powers themselves and $U_{\lambda+1} = A_\lambda \times U_\lambda$. Then
  $\gamma_{\lambda+1}$ can be described as relating those pairs of pairs
  $\big((a_1,a_2),(b_1,b_2)\big)$ and $\big((a'_1,a'_2),(b'_1,b'_2)\big)$ that satisfy:
  $a_1 = a'_1$, $a_2 = a'_2$, and if $a_2\not\in U_\lambda$ then $b_1 =
  b'_1$ and $b_2 = b'_2$. Rewriting this condition, we get
  \begin{multline*}
    \big(\big((a_1,a_2),(b_1,b_2)\big),
    \big((a'_1,a'_2),(b'_1,b'_2)\big)\big)
    \in \gamma_{\lambda+1}
    \text{ if and only if }
    \\
    a_1 = a'_1  \text{, }
    a_2 = a'_2  \text{, }
    \big((a_2,b_1),(a'_2,b'_1)\big) \in \gamma_\lambda \text{, and }
    \big((a_2,b_2),(a'_2,b'_2)\big) \in \gamma_\lambda
  \end{multline*}
  which is clearly compatible with the operations, since $\gamma_\lambda$ is.
  Also note, that if we restrict $\gamma_{\lambda+1}$ to $\al A_{\lambda} \times
  \al B_{\lambda}$, we get $\gamma_\lambda$. For limit ordinals $\lambda$, the
  compatibility of $\gamma_\lambda$ is obtained by a~standard compactness
  argument.
  This way, we obtain for every $\lambda$ a~very special pentagon $\rel
  P'_\lambda$ with $P'_\lambda = A_\lambda \times B_\lambda$ and $\gamma^{\rel
  P_\lambda} = \gamma_\lambda$.

  The final step is to prove that the pentagon $\rel P'_\lambda$ is isomorphic
  to $\rel P_\kappa$ for $\kappa = |\lambda|$. Indeed, $P_\kappa = \kappa \times
  \kappa$, $P'_\lambda = A_\lambda \times B_\lambda$ with $|A_\lambda| =
  |B_\lambda| = \kappa$. Also compare
  \[
    \gamma^{\rel P_\kappa} = \{ ((a,b),(a',b')) :
      \text{ $a = a'$ and if $a\not\in U_\kappa$ then $b = b'$ } \}
  \]
  with the above definition of $\gamma_\lambda$. From these observations, it is
  immediate that any bijection $a\times b\colon \kappa \times \kappa \to
  A_\lambda \times B_\lambda$ defined by components $a$ and $b$ such that $a$
  maps $U_\kappa$ onto $U_\lambda$ is an isomorphism between $\rel P_\kappa$ and
  $\rel P'_\lambda$.
\end{proof}

\subsection{Proof of Theorem~\ref{thm:overview-introduction}(i)}
  Given a~variety $\var V$ that is not congruence modular, we distinguish
  between two cases: $\var V$ is linear; and $\var V$ is idempotent. If it is
  the first case, we get from Proposition~\ref{prop:coloring-for-modularity} and
  Lemma~\ref{lem:linear-varieties} that $\var V$ contains an algebra compatible
  with a~very special pentagon $\rel P^{\var V} = \rel P_0$. In the second case,
  $\var V$ is idempotent, we obtain a~very special pentagon $\rel P^{\var V}$
  compatible with an~algebra from $\var V$ by
  Corollary~\ref{cor:idempotent-special-pentagon}.

  Given that both $\var V$ and $\var W$ are not congruence modular, and either
  linear, or idempotent, we have very special pentagons $\rel P^{\var V}$ and
  $\rel P^{\var W}$ compatible with algebras from the corresponding varieties.
  Choosing an infinite cardinal $\kappa$ larger than the sizes of both $P^{\var
  V}$ and $P^{\var W}$.  Proposition~\ref{prop:huge-pentagons} yields that both
  varieties contain algebras compatible with the pentagon $\rel P_\kappa$,
  therefore by Lemma~\ref{lem:joins}, the interpretability join $\var V \join
  \var W$ contains an algebra compatible with $\rel P_\kappa$ witnessing that
  $\var V \join \var W$ is not congruence modular. \qed

\subsection{Cofinal chain}

Here we investigate properties of the transfinite sequence
\(\clo P_0\), \(\clo P_{\aleph_0}\), \(\clo P_{\aleph_1}, \dots\)
of polymorphism clones of pentagons $\rel P_0, \rel P_{\aleph_0}, \dots$;
in particular, we show that this sequence form a~strictly increasing chain in
the lattice of clones, and as a~corollary thereof, we obtain that there is no
maximal (in the interpretability order) idempotent variety that is not
congruence modular. For the rest of this section, $\kappa$ and $\lambda$ will
denote either infinite cardinals, or $0$.

The fact that the chain is increasing follows from the following corollary of
Proposition~\ref{prop:huge-pentagons}.

\begin{corollary} \label{cor:modularity-chain}
There is a~clone homomorphisms from $\clo P_\lambda$ to $\clo P_\kappa$ for all
$\lambda \leq \kappa$.
\end{corollary}

\begin{proof}
  If we use Proposition~\ref{prop:huge-pentagons} on the variety of actions of
  $\clo P_\lambda$, we obtain that it contains an algebra compatible with $\rel
  P_\kappa$. But such an algebra corresponds to an~action of $\clo P_\lambda$ on
  $\rel P_\kappa$ by polymorphisms, therefore to a~clone homomorphism from $\clo
  P_\lambda$ to $\clo P_\kappa$.
\end{proof}

Before we proceed further, we need a~better description of polymorphisms of the
pentagons $\rel P_\kappa$.

\begin{lemma} \label{lem:compatibility-with-blocker}
Let $\kappa$ be an~infinite cardinal, and consider the pentagon $\rel P_\kappa$,
and let $f\colon P_\kappa^k \to P_\kappa$ be a~functions defined by components
$f^1,f^2\colon \kappa^k \to \kappa$. Then the following are equivalent:
\begin{enumerate}
  \item $f$ is a polymorphism of $\rel P_\kappa$,
  \item if $f^1 (a_1,\dots,a_n) \notin U$ for some $a_1$, \dots, $a_n \in
  \kappa$ then $f^2$ does not depend on any coordinate $i$ such that $a_i \in
  U$.
\end{enumerate}
\end{lemma}

\begin{proof}
  $(1)\to(2)$\quad Since $f$ is defined component-wise, if is compatible with
  $\alpha^{\rel P_\kappa}$ and $\beta^{\rel P_\kappa}$. For compatibility with
  $\gamma^{\rel P_\kappa}$, recall that $((a,b),(a',b')) \in \gamma^{\rel
  P_\kappa}$ if and only if $a = a'$, and $a\in U$ or $b = b'$.  Therefore, if
  $f$ is compatible with $\gamma^{\rel P_\kappa}$ and $f^1 (a_1,\dots,a_n)
  \notin U$ then
  \(
    f^2 (b_1,\dots,b_n) =
    f^2 (b'_1,\dots,b'_n)
  \)
  for all $b_i$, $b_i'$ where $b_i = b_i'$ whenever $a_i \notin U$ (observe that
  $((a_i,b_i),(a_i,b_i'))\in \gamma^{\rel P_\kappa}$ for all $i$).  This means
  that $f^2$ does not depend on any coordinate $i$ with $a_i \in U$.
  The implication $(2) \to (1)$ is given by reversing this argument.
\end{proof}

Instead of proving that the chain of $\clo P_\kappa$'s is strictly increasing,
we prove the following stronger statement which will allow us to show that there
is no maximal (in the ordering by interpretability) idempotent non-modular
variety.

\begin{proposition} \label{prop:modularity-large-algebras}
  The idempotent reducts of $\clo P_\kappa$'s form a~strictly increasing chain
  in the lattice of clones.
\end{proposition}

\begin{proof}
Let $\idclo P_\kappa$ denote the idempotent reduct of $\clo P_\kappa$. We need
to show two facts: (1) there is a~clone homomorphism from $\idclo P_\lambda$ to
$\idclo P_\kappa$ for all $\lambda \leq \kappa$, and (2) there is no clone
homomorphism from $\idclo P_\kappa$ to $\idclo P_\lambda$ for any $\lambda <
\kappa$. (1) follows directly from Corollary \ref{cor:modularity-chain}, since any clone
homomorphism from $\clo P_\lambda$ to $\clo P_\kappa$ has to preserve idempotent
functions.

To prove (2) suppose that $\kappa > \lambda$. We will find identities that are
satisfied in $\idclo P_\kappa$ but are not satisfiable in $\idclo P_\lambda$. In
fact they are not satisfiable in any clone on a~set of cardinality strictly
smaller than $\kappa$ except the~one-element set.
The identities use binary symbols $f_i$ for $i\in \kappa$ and ternary symbols
$p_{i,j}$, $q_{i,j}$, $r_{i,j}$ for $i,j \in \kappa$, $i\neq j$:
\begin{align*}
  x &\equals p_{i,j}(x,f_j(x,y),y), \\
  p_{i,j}(x,f_i(x,y),y) &\equals q_{i,j}(x,f_j(x,y),y), \\
  q_{i,j}(x,f_i(x,y),y) &\equals r_{i,j}(x,f_j(x,y),y), \\
  r_{i,j}(x,f_i(x,y),y) &\equals y
\end{align*}
for all $i\neq j$, and $f_i(x,x) \equals x$ for all $i$.

We will define functions in $\idclo P_\kappa$ that satisfy these identities
coordinatewise (recall $P_\kappa = \kappa \times \kappa$); this will assure that
these functions are compatible with $\alpha^{\rel P_\kappa}$ and $\beta^{\rel
P_\kappa}$. We fix $c^1 \in U_\kappa$ and $c^2 \in \kappa$. The functions $f_i$
are defined, unless required by idempotence otherwise, as constants while
choosing different constants for different $i$'s. In detail, we pick $c^1_i \in
U_\kappa$ to be pairwise distinct, and $c^2_i \in \kappa$ as well. Then
\(
  f_i^u(x,y) = c^u_i
\) if $x\neq y$ and $f_i^u(x,x) = x$ for $u=1,2$.
The components of $p_{i,j}$, $q_{i,j}$, and $r_{i,j}$ are defined as
follows:
\[
\arraycolsep=0pt
\begin{array}{rlrl}
    p^1_{i,j}(x,y,z) &{}= \begin{cases}
      x & \text{if $y = f_j^1(x,z)$,}\\
      c_1 & \text{otherwise;}
    \end{cases}
  &\quad
    p^2_{i,j}(x,y,z) &{}= x;
  \\
    q^1_{i,j} (x,y,z) &{}= \begin{cases}
      x & \text{if $x = y = z$,} \\
      c_1 & \text{otherwise;}
    \end{cases}
  &\quad
    q^2_{i,j} (x,y,z) &{}= \begin{cases}
      x & \text{if $y = f_j^2 (x,z)$,} \\
      z & \text{if $y = f_i^2 (x,z)$,} \\
      c_2 & \text{otherwise;}
    \end{cases}
  \\
    r^1_{i,j}(x,y,z) &{}= \begin{cases}
      z & \text{if $y = f_i^1(x,z)$,}\\
      c_1 & \text{otherwise;}
    \end{cases}
  &\quad
    r^2_{i,j}(x,y,z) &{}= z;
\end{array}
\]
It is straightforward to check that these functions satisfy the identities. The
compatibility with $\alpha^{\rel P_\kappa}$ and $\beta^{\rel P_\kappa}$ is
immediate from defining them component-wise. The compatibility with $\gamma^{\rel
P_\kappa}$ follows from Lemma~\ref{lem:compatibility-with-blocker}: the only
case when $p^1_{i,j}(x,y,z) \notin U_\kappa$ is when $x \notin U_\kappa$ and $y
= f_j^1(x,z)$ but $p^2_{i,j}$ does not depend on the second and third variable,
therefore $p_{i,j}$ is compatible; the argument for $r_{i,j}$ is analogous;
$q_{i,j}^1(x,y,z)$ falls in $U_\kappa$ unless $x = y = z \notin U_\kappa$, so
$q_{i,j}$ is also compatible.

Next, we claim that the above identities are not satisfied in any non-trivial
algebra $\al A$ of size strictly less than $\kappa$.
Indeed, if $A$ contains two distinct elements $a$ and $b$, then $f_i(a,b)$,
$i\in \kappa$ are pairwise distinct, since if $f_i^{\al A}(a,b) = f_j^{\al
A}(a,b)$, then
\begin{multline*}
  a
  = p^{\al A}_{i,j}(a,f_j(a,b),b)
  = p^{\al A}_{i,j}(a,f_i(a,b),b)
  = q^{\al A}_{i,j}(a,f_j(a,b),b)
  = \\ q^{\al A}_{i,j}(a,f_i(a,b),b)
  = r^{\al A}_{i,j}(a,f_j(a,b),b)
  = r^{\al A}_{i,j}(a,f_i(a,b),b)
  = b.
\end{multline*}
This shows in particular that these identities are not satisfiable in $\idclo
P_\lambda$.
\end{proof}

\begin{corollary} \label{cor:no-strongest-non-modular-variety}
The class of all interpretability classes of idempotent varieties that are not
congruence modular does not have a~largest element.
\end{corollary}

\begin{proof}
  For a~contradiction, suppose that there is a~largest interpretability class
  among those containing a~non-modular idempotent variety, and let $\var V$ be
  a~variety from this class. By Corollary~\ref{cor:idempotent-special-pentagon}
  and Proposition~\ref{prop:huge-pentagons}, we know that $\var V$ is
  interpretable in the variety of actions of $\clo P_\kappa$ for some $\kappa$.
  Fix any such $\kappa$, and let $\lambda > \kappa$ be a~cardinal. By the
  maximality of $\var V$, we have that the variety of actions of $\idclo
  P_\lambda$ (the idempotent reduct of $\clo P_\lambda$) is interpretable in
  $\var V$, therefore also in the variety of actions of $\clo P_\kappa$ which
  contradicts the previous proposition.
\end{proof}
