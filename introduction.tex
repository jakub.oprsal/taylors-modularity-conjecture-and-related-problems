\section{Introduction}

An~\emph{interpretation} $\iota$ of a~variety $\var V$ in a~variety $\var W$ is
a~mapping that maps basic operations of $\var V$ to terms of $\var W$ of the
same arity such that for every algebra $\al A\in \var W$, the algebra $(A,
(\iota(f)^{\al A})_{f\in \sigma})$ (where $\sigma$ is the signature of $\var V$)
is an algebra in $\var V$.  We say that a~variety $\var V$ is interpretable in
a~variety $\var W$ if there exist an~interpretation of $\var V$ in $\var W$. The
\emph{lattice of interpretability types of varieties} (see
\cite{neumann74,garcia.taylor84}) is then constructed by quasi-ordering all
varieties by interpretability, and factoring out varieties that are
interpretable in each other. This gives a~partially ordered class such that
every set has a~join and a~meet.  The lattice of interpretability types of
varieties is a~suitable object for expressing properties of Mal'cev
conditions~(for a~formal definition see \cite{taylor73}): The varieties that
satisfy a~given Mal'cev condition form a~filter in this lattice, thus, e.g.\
implications among Mal'cev conditions translate into inclusions among the
corresponding filters. 

In this paper, we contribute to the line of research whose aim is to understand
which of the important Mal'cev conditions are indecomposable in the following
strong sense: if two sets of identities in disjoint languages together imply
the Mal'cev condition, then one of the sets already do. An equivalent
formulation using the interpretability lattice is especially simple: which of
the important Mal'cev conditions determine a~prime filter?
Some of the Mal'cev conditions with this property have been described in the
monograph by Garcia and Taylor \cite{garcia.taylor84}, e.g.\  having a~cyclic
term of given prime arity. Garcia and Taylor conjectured that the filter of
congruence permutable varieties and the filter of congruence modular varieties
are prime. For congruence permutability, this was confirmed by
Tschantz~\cite{tschantz96}. Unfortunately, this proof has never been published.
The congruence modular case is still open:

\begin{conjecture}[Taylor's modularity conjecture]
  The filter of congruence modular varieties is prime, that is, if $\var V$ and
  $\var W$ are two varieties such that $\var V \join \var W$ is congruence
  modular, then either $\var V$ or $\var W$ is congruence modular.
\end{conjecture}

In \cite{bentz.sequeira14}, Bentz and Sequeira proved that this is true if $\var
V$ and $\var W$ are idempotent varieties that can be defined by linear
identities (such varieties are called linear idempotent varieties), and later in
\cite{barto.oprsal.ea15}, Barto, Pinsker, and the author generalized their
result to linear varieties that do not need to be idempotent. In this paper we
generalize Bentz and Sequeira's result in a~different direction.

\begin{theorem} \label{thm:taylors-conjecture-introduction}
  If $\var V$, $\var W$ are two idempotent varieties such that $\var V \join
  \var W$ is congruence modular then either $\var V$ or $\var W$ is congruence
  modular.
\end{theorem}

Several similar partial results on primeness of some Mal'cev filters have been
obtained before. Bentz and Sequeira in \cite{bentz.sequeira14} also proved for
two linear idempotent varieties: if the join of the two varieties is congruence
$n$-permutable for some $n$, then so is one of the two varieties; and
similarly if their join satisfies a~non-trivial congruence identities, then so
does one of the two varieties. Stronger versions of these results also follow
from the work of Valeriote and Willard \cite{valeriote.willard14}, who proved
that every idempotent variety that is not $n$-permutable for any $n$ is
interpretable in the variety of distributive lattices, and the work of Kearnes
and Kiss \cite{kearnes.kiss13}, who proved that any idempotent variety which
does not satisfy a~non-trivial congruence identity is interpretable in the
variety of semilattices. Recently, a~similar result has been obtained by Kearnes
and Szendrei \cite{kearnes.szendrei15} for the filter of varieties having a~cube
term.

\begin{theorem} \label{thm:cube-terms-introduction}
  Suppose that $n\geq 2$, and let $\var V$ and $\var W$ be two idempotent
  varieties such that $\var V \join \var W$ has an~$n$-cube term.  Then so does
  either $\var V$ or~$\var W$.
\end{theorem}

We provide an alternative proof of this result using the fact (obtained in
\cite{kearnes.szendrei15} and recently also by \cite{mckenzie.moore17}) that
idempotent varieties that do not have a~cube term contain an algebra with a~cube
term blocker.

All of the filters mentioned so far share the following property: the varieties
from their complements have a~so-called strong coloring of their terms by
a~finite relational structure that depends on the particular filter. The precise
definition is given in Section~\ref{sec:3.2}. The notion is a~reformulation of
colorings described in \cite{barto.oprsal.ea15}, and a~generalization of
compatibility with projections introduced in \cite{sequeira01}. In the present
manuscript, we describe these coloring structures.
These characterizations by the means of colorings allow us to give analogous
results for linear varieties. Moreover, we are also able to connect these
results with their analogues for idempotent varieties (when those are available).

\begin{theorem} \label{thm:overview-introduction} \label{thm:1.4}
  Let $\var V$ and $\var W$ be two varieties such that each of them is either
  linear or idempotent.
  \begin{itemize}
  \item[(i)] If $\var V\join \var W$ is congruence modular, then so is either
    $\var V$ or $\var W$;
  \item[(ii)] if $\var V\join \var W$ is congruence $k$-permutable for some $k$,
    then so is either $\var V$ or $\var W$;
  \item[(iii)] if $\var V\join \var W$ satisfies a~non-trivial congruence
    identity, then so does either $\var V$ or $\var W$;
  \item[(iv)] for all $n\geq 2$: if $\var V \in \var W$ has an~$n$-cube term,
    then so does either $\var V$ or $\var W$.
  \end{itemize}
\end{theorem}

\begin{theorem} \label{thm:permutability-introduction} \label{thm:1.5}
  Let $n\geq 2$. If $\var V$ and $\var W$ are two linear varieties such that
  $\var V \join \var W$ is congruence $n$-permutable, then so is either $\var
  V$ or $\var W$.
\end{theorem}

However, we are not able to answer the following. (Note that the case $n=2$ has
been resolved in \cite{tschantz96} as well as Theorem~\ref{thm:1.4}(iv).)

\begin{problem} Given $n > 2$ and two idempotent varieties $\var V$ and $\var
W$ such that $\var V \join \var W$ is $n$-permutable. Is it always true that
either $\var V$ or $\var W$ is $n$-permutable?
\end{problem}
