# Taylor's modularity conjecture and related problems for idempotent varieties

## Author(s)
Jakub Opršal

## Abstract
We provide a partial result on Taylor's modularity conjecture, and several
related problems. Namely, we show that the interpretability join of two
idempotent varieties that are not congruence modular is not congruence modular
either, and we prove an analogue for idempotent varieties with a cube term.
Also, similar results are proved for linear varieties and the properties of
congruence modularity, having a cube term, congruence $n$-permutability for
a fixed $n$, and satisfying a non-trivial congruence identity.

## References
This have been submitted to arXiv as
[arxiv.org/abs/1602.08639](https://arxiv.org/abs/1602.08639), and Order (in the
review process).
