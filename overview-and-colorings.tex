\section{Overview of the method}

Each of the Mal'cev filters given by one of the conditions mentioned in
Theorems~\ref{thm:1.4} and~\ref{thm:1.5} can be described as the class of
varieties that do not contain an algebra that has some compatible relations with
a~special property (e.g.\ congruence modular are those varieties that do not
contain an algebra having three congruences that do not satisfy the modular law,
finitely generated varieties with cube terms are those that do not contain an
algebra with `too many subpowers', etc.). In order to prove that these filters
are prime, we have to prove that for any two varieties $\var V$, $\var W$ that
contain such `ugly' algebras, also $\var V \join \var W$ contains such an
algebra. More precisely, Taylor's modularity conjecture in fact states that for
any two varieties $\var V$, $\var W$ that are not congruence modular, there
exists an algebra $\al A$ in $\var V \join \var W$ that has congruences
$\alpha$, $\beta$, and $\gamma$ that do not satisfy the modularity law. Such an
algebra has two natural reducts $\al A_1 \in \var V$, $\al A_2 \in \var W$ which
are obtained by taking only those basic operations of $\al A$ that belong to the
signature of the respective variety. Both these algebras share a~universe and
the three congruences $\alpha$, $\beta$, and $\gamma$. Therefore, Taylor's
conjecture states that for any two congruence non-modular varieties, there exist
a~set $A$ and  equivalence relations $\alpha$, $\beta$, $\gamma$ on $A$ such
that both varieties contain an algebra with the universe $A$ and congruences
$\alpha$, $\beta$, and $\gamma$.  In other words (see Lemma~\ref{lem:2.1}), both
varieties are interpretable in the variety of actions of the polymorphism clone
$\clo A$ of the relational structure $(A; \alpha, \beta, \gamma)$.
Although it would be convenient, we cannot find a~single relational structure
$\rel B$ such that a~variety is not congruence modular if and only if it is
interpretable in the variety of actions of~$\clo B$. This is due to the fact,
that congruence non-modular varieties can omit algebras of size smaller then any
fixed cardinal. Instead, we will find a~chain of clones $\clo P_\kappa$ indexed
by cardinals, that are polymorphism clones of relational structures of
increasing sizes, such that every non-modular idempotent variety is
interpretable in the variety of actions of $\clo P_\kappa$ for every big enough
cardinal $\kappa$. The same method is also applied for the other filters in the
interpretability lattice with the only difference being use of different
relational structures.

Contrary to the general case, in the case that a~variety $\var V$ is linear and
not congruence modular, we are able to prove that it is interpretable in the
variety of actions of the clone $\clo P_0$, that is defined as the clone of
polymorphisms of $\rel P_0 = (\{ 0,1,2,3 \}, \alpha, \beta, \gamma )$ where
$\alpha$, $\beta$, and $\gamma$ are equivalences defined by partitions $01|23$,
$03|12$, and $0|12|3$, respectively. This is due to the fact that non-modular
varieties can be described as those varieties whose terms are colorable by $\rel
P_0$ (see Proposition~\ref{prop:coloring-for-modularity}). This turns out to be,
in the case of linear varieties, equivalent to any of the conditions in
Lemma~\ref{lem:2.1} for the relational structure $\rel P_0$ (see
Lemma~\ref{lem:linear-varieties}). Moreover, we will also show that there is
a~clone homomorphism from $\clo P_0$ to $\clo P_\kappa$ for any infinite
cardinal $\kappa$, connecting the results for linear varieties and idempotent
varieties. 

The rest of this section is dedicated to describing several general results
to be used in the following sections.

\subsection{Varieties of actions of polymorphism clones}

Our method is based on the following lemma that describes the class of varieties
that are interpretable in the~variety of actions of the polymorphism clone of
a~relational structure. Here, we denote the signature of a~variety $\var V$ by
$\sigma(\var V)$.

\begin{lemma} \label{lem:2.1}
  Let $\var V$ be a~variety, $\clo F$ the clone of its countably generated free
  algebra, $\rel B$ be a~relational structure.  The
  following are equivalent:
  \begin{enumerate}
    \item There is a~clone homomorphism from $\clo F$ to $\clo B$;
    \item $\var V$ is interpretable in the variety of actions of $\clo B$;
    \item $\var V$ contains an algebra compatible with $\rel B$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  (1) $\to$ (2): Let $\al F$ denote the countable generated free algebra, and
  let $\xi\colon \clo F \to \clo B$ be a~clone homomorphism. We
  will use it to define an interpretation $\iota$ from $\var V$ to the variety
  of action of $\clo B$. Note that the signature of the variety of actions of
  $\clo B$ is the set $\clo B$. For a~$k$-ary symbol $f$ in the signature of
  $\var V$, we define
  \(
    \iota(f) = \xi(f^{\al F})
  \).
  We get that $(B; \iota(f)_{f\in \sigma(\var V)}) \in \var V$ since $\xi$ preserves
  identities, and consequently, since $(B; (f)_{f\in \clo B})$ generates the
  variety of actions of $\clo B$, $\iota$ is an interpretation.

  (2) $\to$ (3): Suppose that $\iota$ is an interpretation of $\var V$ in the
  variety $\var W$ of actions of $\clo B$.
  Note that, since $\clo B$ is closed under compositions, we can without loss of
  generality suppose that $\iota$ maps basic operations of $\var V$ to basic
  operations of $\var W$ (i.e., the elements of $\clo B$).
  From the definition of an interpretation, we have that for any action of $\clo
  B$ on a~set $C$, $(C,(\iota(f))_{f\in \sigma(\var V)})$ is an algebra in $\var
  V$ (here $\iota(f)$ is understood as the action of the~element $\iota (f) \in
  \clo B$). Considering in particular the natural action of $\clo B$ on $B$, we
  claim that $\al A = (B,(\iota(f))_{f\in \sigma(\var V)})$ has the required
  properties.  Indeed, it has the right universe, and moreover for any $f\in
  \sigma(\var V)$, the function $\iota(f)$ is a~polymorphism of~$\rel B$.

  (3) $\to$ (1): Let $\al A$ be the algebra satisfying the condition $(3)$, and
  let $\al F$ be the countably generated free algebra in $\var V$. Define
  $\xi\colon \clo F \to \clo B$ by putting $\xi(f^{\al F}) = f^{\al A}$ for
  every term $f$ of $\var V$. This mapping is well-defined since $\al A$
  satisfies all identities that are satisfied in $\al F$ and every term
  operation of $\al A$ is a~polymorphism of $\rel B$. It is also a~clone
  homomorphism since it preserves projections and composition.
\end{proof}

Also from the above, we get the following property of the interpretability join
of two varieties.

\begin{lemma} \label{lem:joins}
  Let $\rel B$ be a~relational structure, and let $\var V$ and $\var W$ be two
  varieties such that both contain an algebra compatible with $\rel B$. Then
  also the interpretability join $\var V \join \var W$ contains an algebra
  compatible with $\rel B$.
\end{lemma}

\begin{proof}
  From the previous lemma, we get that both $\var V$ and $\var W$ are
  interpretable in the variety of actions of $\clo B$, therefore also $\var V
  \join \var W$ is by the definition of interpretability join. Again by the
  previous lemma, this implies that $\var V\join \var W$ contain an algebra
  compatible with $\rel B$.

  Alternatively, we can construct such an algebra directly: Suppose that
  $\al C \in \var V$ and $\al D \in \var W$ are compatible with
  $\rel B$ (in particular $C = D = B$). The algebra $\al B$ in $\var V\join \var W$
  compatible with $\rel B$ is obtained by putting the structures of these two
  algebras on top of each other, i.e., as
  $\al B = (B; (f^{\al C})_{f\in \sigma(\var V)}, (g^{\al D})_{g\in \sigma(\var
  W)})$.
\end{proof}

\subsection{Colorings of terms by relational structures} \label{sec:3.2}

Usually we are unable to find a~single relational structure $\rel B$ such that
variety falls in the complement of a filter if and only if it is interpretable
in the variety of actions of $\clo B$.
Nevertheless, we can relax from taking~clone homomorphism, corresponding to
interpretation of varieties, to h1 clone homomorphisms. An h1 clone homomorphism
is a~mapping between two clones that preserves identities of height~1 in a
similar way as clone homomorphism preserves all identities
\cite[Definition 5.1]{barto.oprsal.ea15}.
Existence of an~h1 clone homomorphism from the clone of a~variety $\var V$ to the
polymorphism clone of a~relational structure $\rel B$ is described by an
existence of a~coloring described below.
As noted in \cite{barto.oprsal.ea15}, h1 clone homomorphisms encapsulate not
only the usual algebraical (primitive positive) construction, but also
homomorphic equivalence of relational structures. This said, coloring is defined
as a~homomorphism from a~certain `freely generated' relational structure given
by $\var V$ and $\rel B$ to the relational structure $\rel B$. There will always
be a~natural homomorphism from $\rel B$ to this freely generated structure.

\begin{definition} \label{def:free-structure}
  Given a~variety $\var V$ and $\rel B$ a~relational structure.  We define
  a~\emph{free structure generated by $\rel B$} to be a~relational structure
  $\rel F$ of the same signature as $\rel B$ whose underlying set is the
  universe of the free $B$-generated algebra $\al F$, and a~relation $R^{\rel
  F}$ is the smallest compatible relation on $F$ containing $R^{\rel B}$.
\end{definition}

As for a~free algebra, the elements of $\rel F$ are represented by terms of
$\var V$ over the set $B$ as their set of variables. Therefore, it is sensible
to denote the elements of the copy of $B$ in $F$ as
variables $x_b$, $b\in B$. Another way is to define $\al F$ as
being generated by the set $\{ x_b : b\in B \}$ rather then $B$ itself, and
$R^{\rel F}$ being the relations generated by $\{ (x_{b_1},\dots,x_{b_k}) :
(b_1,\dots,b_k) \in R^{\rel B} \}$. It is clear that both relational structures
are isomorphic, moreover they are also isomorphic as (mixed) structures
with both relations $R^{\rel F}$ and operations $f^{\al F}$.

\begin{definition} \label{def:coloring}
  A~\emph{coloring of terms} of a~variety $\var V$ by $\rel B$ (or \emph{$\rel
  B$-coloring of terms}) is any~homomorphism from the free structure
  generated by $\rel B$ to $\rel B$. A~coloring is \emph{strong} if its
  restriction to $B$ is an indentity mapping (maps $x_b$ to $b$).
\end{definition}

The following is a~consequence of the theory developed
in~\cite{barto.oprsal.ea15}; it follows from Propositions 7.4 and 7.5 of the
mentioned paper. Nevertheless, we present a~variant of the proof of equivalence
of (3) and (5) for completeness.

\begin{lemma} \label{lem:linear-varieties}
  Let $\var V$ be a~linear variety, $\clo F$ its clone, and $\rel B$
  a~relational structure.
  The following are equivalent:
  \begin{enumerate}
    \item There is a~clone homomorphism from $\clo F$ to $\clo B$;
    \item there is a~strong h1 clone homomorphism from $\clo F$ to $\clo B$;
    \item the terms of $\var V$ are strongly $\rel B$-colorable;
    \item $\var V$ is interpretable in the variety of actions of $\clo B$;
    \item $\var V$ contains an algebra compatible with $\rel B$.
  \end{enumerate}
\end{lemma}

\begin{proof} The equivalence of (1), (4), and (5) follows from
Lemma~\ref{lem:2.1}.
The implication $(1) \to (2)$ is trivial, and its converse is given by
\cite[Proposition 7.5]{barto.oprsal.ea15}.
To finish the proof, we show that (3) and (5) are equivalent.
We denote $\al F_B$, the free algebra in $\var V$ generated by
$B$, and $\rel F_B$, the free structure generated by $\rel B$. 

$(3) \to (5)$: A~strong coloring
is a~mapping $c\colon F_B \to B$. We define a~structure of a~$\var V$-algebra on
the set $\rel B$ as a~retraction (see \cite[Definition 4.1]{barto.oprsal.ea15})
of $\al F_B$ by $i$ and $c$ where $i\colon B \to F_B$ is an inclusion ($i(b) =
x_b$, resp.), i.e.,
\[
  f^{\al B} (b_1,\dots,b_n) = c( f^{\al F_B} ( x_{b_1}, \dots, x_{b_n} ) ).
\]
One can observe that operations defined this way satisfy all linear identities
satisfied by $\al F_B$ (see also \cite[Corollary 5.4]{barto.oprsal.ea15}),
therefore $\al B \in \var V$. To prove that $\al B$ is compatible with $\rel B$,
we consider a~relation $R^{\rel B}$ of $\rel B$ and $\tup b_1,\dots,\tup b_n \in
R^{\rel B}$. Then by the definition of $\rel F_B$, $\tup b_1,\dots,\tup b_n \in
R^{\rel F_B}$, therefore $f^{\al F_B} ( \tup b_1, \dots, \tup b_n ) \in R^{\rel
F_B}$, and $c( f^{\al F_B} ( \tup b_1, \dots, \tup b_n ) ) \in R^{\rel B}$ from
the definition of coloring. This shows that $f^{\al B}$ defined as above is
a~polymorphism of $\rel B$, and hence $\al B$ is compatible with $\rel B$.

$(5) \to (3)$: Let $\al B$ be an algebra in $\var V$ compatible with $\rel B$,
and let $c\colon \al F_B \to \al B$ be the natural homomorphism from $\al F_B$
that extends the identity mapping (the mapping $x_b \mapsto b$, resp.). We claim
that this mapping is a~coloring. Indeed, if $\tup f \in R^{\rel F_B}$ then there
is a~term $f$ and $\tup b_1,\dots, \tup b_n \in R^{\rel B}$ such that $\tup f =
f^{\al F_B}( \tup b_1,\dots,\tup b_n)$, therefore
\[
  c( \tup f ) = c( f^{\al F_B}( \tup b_1,\dots,\tup b_n) ) = f^{\al B} (\tup
  b_1,\dots,\tup b_n) \in R^{\rel B},
\]
since $\rel B$ is compatible with $\al B$. The coloring $c$ is strong by
definition.
\end{proof}

\subsection{Coloring by transitive relations}

Some of the relational structures we use for coloring have binary transitive
relations, or even equivalence relations.  In these cases, we can refine the
free structure $\rel F$ generated by a~relational structure $\rel B$ in the
following way.

\begin{lemma} \label{lem:coloring-by-quosets}
  Let $\rel B$ be a~relational structure, $\var V$ a~variety, $\rel F$ the free
  structure in $\var V$ generated by $\rel B$, and let $\rel F'$ be a~structure
  obtained from $\rel F$ by replacing every relation $R^{\rel F}$, for which
  $R^{\rel B}$ is transitive, by its transitive closure. Then a~mapping $c\colon
  F \to B$ is a~coloring if and only if it is a~homomorphism $c\colon \rel F'
  \to \rel B$.
\end{lemma}

\begin{proof}
  This directly follows from the corresponding result about relational
  structures in general. In detail, given that $\rel A$ and $\rel B$ are
  relational structures sharing a~signature such that $R^{\rel B}$ is transitive,
  then every homomorphism from $\rel A$ to $\rel B$ maps transitive closure of
  $R^{\rel A}$ to $R^{\rel B}$. Conversely, if $\rel A'$ is obtained from $\rel A$
  as described in the proposition, then any homomorphism from $\rel A'$ to $\rel
  B$ is automatically a~homomorphism from $\rel A$ to $\rel B$.
\end{proof}

Following the notation of the lemma, note that, if the relation $R^{\rel B}$ is
symmetric binary relation, then also $R^{\rel F}$ is. This follows from
the fact that if $(f,g) \in R^{\rel F}$ then there is a~term $t$ of $\var V$ and
tuples $(a_1,b_1),\dots,(a_n,b_n) \in R^{\rel B}$ such that
\(
  t(a_1,\dots,a_n) = f \) and \(
  t(b_1,\dots,b_n) = g.
\)
Applying the same term $t$ to pairs $(b_1,a_1),\dots,(b_n,a_n) \in R^{\rel B}$,
we get that $(g,f) \in R^{\rel F}$. From this observation and the lemma above,
we get that if $R^{\rel B}$ is an equivalence relation, we can take $R^{\rel F}$
to be the congruence generated by $R^{\rel B}$. This case is further described
in the following lemma.

\begin{lemma} \label{lem:coloring-by-equivalences}
  Let $\rel B$ be a~finite relational structure with $B = \{1,\dots,n\}$,
  $\alpha^{\rel B}$ an~equivalence relation,
  $\al F$ the free $B$-generated algebra,
  and $\rel F$ the free structure in $\var V$ generated by $\rel B$. Then the
  the following three definitions give the same relation:
  \begin{enumerate}
    \item the transitive closure of $\alpha^{\rel F}$,
    \item the congruence of $\al F$ generated by $\alpha^{\rel B}$,
    \item the set of all pairs
      \(
        ( f^{\al F}(1,\dots,n) , g^{\al F}(1,\dots,n))
      \)
      where $f,g$ are terms of $\var V$ such that
      \(
        f(x_1,\dots,x_n) \equals g(x_1,\dots,x_n)
      \)
      is satisfied in $\var V$ whenever $x_i \equals x_j$ for all $(i,j) \in
      \alpha^{\rel B}$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  Let us denote the relation defined in the item ($i$) of the above list by
  $\alpha_i$.  Clearly, $\alpha_1 \subseteq \alpha_2$ since $\alpha_2$ is
  transitive and contains $\alpha^{\rel F}$. We will prove $\alpha_2 \subseteq
  \alpha_3 \subseteq \alpha_1$.

  $\alpha_2 \subseteq \alpha_3$: Clearly, $\alpha_3$ is symmetric,
  transitive, and reflexive. It is straightforward to check that it is
  compatible with operations of $\al F$.
  Finally, we claim that $\alpha^{\rel B} \subseteq \alpha_3$. Indeed, pick
  $(i,j) \in \alpha^{\rel B}$ and choose $f(x_1,\dots,x_n) = x_i$ and
  $g(x_1,\dots,x_n) = x_j$. Clearly, $f$ and $g$ satisfies the condition in (3),
  therefore $(i,j) = (f^{\al F}(1,\dots,n),g^{\al F}(1,\dots,n)) \in \alpha_3$.
  We proved that $\alpha_3$ is a~congruence of $\al F$ containing $\alpha^{\rel
  B}$, which concludes $\alpha_2 \subseteq \alpha_3$.

  $\alpha_3 \subseteq \alpha_1$: Let $(f^{\al F}(1,\dots,n),g^{\al
  F}(1,\dots,n))$ be a~typical pair in $\alpha_3$. Fix a~representative for each
  $\alpha^{\rel B}$ class, and let $i_k$ denote the representative of the class
  containing~$k$. From the definition of $\alpha^{\rel F}$, we obtain
  \(
    ( f^{\al F}(1,\dots,n), f^{\al F}({i_1},\dots,{i_n}) )
      \in \alpha_{\rel F}
  \) and 
  \(
    ( g^{\al F}({i_1},\dots,{i_n}), g^{\al F}(1,\dots,n) )
    \in \alpha_{\rel F}
  \).
  Since $f(x_{i_1},\dots,x_{i_n}) \equals g(x_{i_1},\dots,x_{i_n})$ is true
  in $\var V$, we get that $f^{\al F}({i_1},\dots,{i_n}) = g^{\al
  F}({i_1},\dots,{i_n})$, and consequently
  \[
    \bigl( f^{\al F}(1,\dots,n), g^{\al F}(1,\dots,n) \bigr) \in
      \alpha^{\rel F} \circ \alpha^{\rel F} \subseteq \alpha_1
  \]
  which concludes the proof of $\alpha_3 \subseteq \alpha_1$.
\end{proof}

As a~consequence of the previous lemma, we also obtain that coloring by
a~relational structure $\rel B$ whose all relations are equivalence relation is
equivalent to having terms compatible with projections as defined in
\cite{sequeira06} (compare item (3) with Definition 5.2 of \cite{sequeira06}).
The corresponding set $(\alpha_1,\dots,\alpha_k)$ of projections can be obtained
from the relations of $\rel B$.


\subsection{Tarski's construction}
  \label{sec:tarski-construction}

In this section we describe a~transfinite construction that appeared in
\cite{burris71} and is attributed to Tarski. This construction is an algebraical
version of L\"owenheim-Skolem theorem, and we will use it to show that the
chains of clones $\clo P_\kappa$, $\clo B_\kappa$, and $\clo C_\kappa$ defined in
the following sections are indeed chains as ordered by an existence of a~clone
homomorphism.

Given an~algebra $\al A$, we define a~sequence $\al A_\lambda$ indexed by
ordinals by the following transfinite construction:
We start with $\al A_0 = \al A$. For an ordinal successor $\lambda + 1$ define
$\al A_{\lambda+1}$ as an algebra isomorphic to $\al A_\lambda^2$ while
identifying the diagonal with $\al A_\lambda$, i.e., take $A_{\lambda+1}
\subseteq A_\lambda$ with a~bijection $f_\lambda\colon A_\lambda^2 \to
A_{\lambda+1}$ such that $f_\lambda(a,a) = a$, and define the structure of $\al
A_{\lambda+1}$ in such a way that $f_\lambda$ is an~isomorphism.
For a~limit ordinal $\lambda$, we set $\al A_\lambda =
\Union_{\alpha < \lambda} \al A_\alpha$.
This construction produces algebras in the variety generated by $\al A$ of all
infinite cardinalities larger than $|A|$. Indeed, one can observe that
$|A_\lambda| = \max\{ |\lambda|, |A| \}$ for all infinite $\lambda$.
We can also follow this construction to get from a~fixed proper subset
$U\subset A$, $U\neq \emptyset$ a~set $U_\lambda \subseteq A_\lambda$ such that both its
cardinality and cardinality of its complement is $|\lambda|$ (given that
$|\lambda| \geq |A|$):
We put $U_0 = U$;
for ordinal successor $\lambda+1$, we take $U_{\lambda + 1} = f_\lambda( A_\lambda
\times U_\lambda )$ (observe that $U_{\lambda+1} \cap A_\lambda = U_\lambda$);
and for limit $\lambda$, we take $U_\lambda = \Union_{\alpha < \lambda}
U_\alpha$.
